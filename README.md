# Instrucciones para instalar y levantar el proyecto
### Tecnologias a instalar

- instalar node, puede descargarlo del siguiente en la siguiente url: [Node](https://nodejs.org/es/ "Node")
- instalar la base de datos Postgresql, puede descargarlo en la siguiente url: [Postgresql](https://www.postgresql.org/download/ "Postgresql")

### Como levantar el proyecto

- Abrir una terminar y dirigirse a la carpeta del proyecto
- Ejecutar el siguente comando: `npm main.js`

### Crear la base de datos
- crear una base de datos con el nombre de: **usuarios**
- dirigirse a la carpeta bd y ejecutar el archivo: **usuario.sql**