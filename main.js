const express = require("express");
const bodyParser = require("body-parser");
const pg = require('pg');

const config = {
  user: 'postgres',
  database: 'usuarios',
  password: 'ahri08',
  host: 'localhost',
  port: 5432,
  ssl: false,
  idleTimeoutMillis: 30000
}

const client = new pg.Pool(config)

// Modelo
class UsuarioModel {
  constructor() {
    this.todos = [];
  }

  async getAllUsers(){
    const res = await client.query('select * from usuario;')
    return res.rows
  }

  getStatus() {
    const nombre = {
      nameSystem: 'apiUsers',
      version: 'v1.0.0',
      developer: 'Deybes Richard Condori Condori',
      email: 'deybes08@gmail.com'
    };
    console.log(nombre);
    return nombre;
  }

  async addUsuario(usuario) {
    const query = 'INSERT INTO usuario(nombre, edad) VALUES($1, $2) RETURNING *';
    const values = [usuario.nombre, usuario.edad]
    const res = await client.query(query, values)
    return res;
  }

  async getAverage() {
    const res = await client.query('select avg(edad) as promedioEdad from usuario;')
    return res.rows[0]
  }
}

// Controlador
class UsuarioController {
  constructor(model) {
    this.model = model;
  }

  async getAllUsers() {
    return await this.model.getAllUsers();
  }

  getStatus() {
    return this.model.getStatus();
  }

  async addUsuario(usuario) {
    await this.model.addUsuario(usuario);
  }

  async getAverage() {
    return await this.model.getAverage();
  }
}

// Vistas (Rutas)
const app = express();
const usuarioModel = new UsuarioModel();
const usuarioController = new UsuarioController(usuarioModel);

app.use(bodyParser.json());

app.get("/users",async  (req, res) => {
  const response = await usuarioController.getAllUsers()
  res.json(response)
});

app.get("/status", (req, res) => {
  const response = usuarioController.getStatus()
  res.json(response)
});

// Vistas (Rutas) (continuación)
app.post("/usuario", (req, res) => {
  const usuario = req.body;
  console.log(req.body)
  usuarioController.addUsuario(usuario);
  res.sendStatus(200);
});

app.get("/average",async  (req, res) => {
  const response = await usuarioController.getAverage()
  res.json(response)
});

app.listen(3000, () => {
  console.log("Server listening on port 3000");
});
